/*!
    \file    main.c
    \brief   running LED

    \version 2019-02-19, V1.0.0, firmware for GD32E23x
    \version 2020-12-12, V1.1.0, firmware for GD32E23x
*/

/*
    Copyright (c) 2020, GigaDevice Semiconductor Inc.

    Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors
       may be used to endorse or promote products derived from this software without
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.
*/

#include "gd32f1x0.h"
#include "systick.h"

#define TEST_PORT GPIOC
#define TEST_PIN GPIO_PIN_3
#define TEST_RCU RCU_GPIOC

/**
 * @brief initial a gpio pin
 *
 */
void test_gpio_initial(void);

void USART_Init(void);

/**
 * @brief
 *
 * @return int
 */
int main(void)
{
	systick_config();

	test_gpio_initial();

	USART_Init();

	while (1) {
		delay_1ms(250);
		gpio_bit_reset(TEST_PORT, TEST_PIN);

		delay_1ms(250);
		gpio_bit_set(TEST_PORT, TEST_PIN);

		/*while (RESET == usart_flag_get(USART1, USART_FLAG_TBE))
			;*/
		usart_data_transmit(USART0, 0xA5);
	}
}

void test_gpio_initial(void)
{
	rcu_periph_clock_enable(TEST_RCU);
	gpio_bit_reset(TEST_PORT, TEST_PIN);
	gpio_mode_set(TEST_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, TEST_PIN);
	gpio_output_options_set(TEST_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, TEST_PIN);
}

void USART_Init(void)
{
	rcu_periph_clock_enable(RCU_GPIOA); // 使能外设时钟
	rcu_periph_clock_enable(RCU_USART0);

	gpio_af_set(GPIOA, GPIO_AF_1, GPIO_PIN_9); // 设置GPIO的备用功能
	gpio_af_set(GPIOA, GPIO_AF_1, GPIO_PIN_10);
	gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_9);	      // 设置GPIO模式
	gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_9); // 设置GPIO输出模式和速度
	gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_10);
	gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_10);

	usart_deinit(USART0);				      // 复位外设USARTx
	usart_word_length_set(USART0, USART_WL_8BIT);	      // 配置USART字长
	usart_stop_bit_set(USART0, USART_STB_1BIT);	      // 配置USART停止位
	usart_parity_config(USART0, USART_PM_NONE);	      // 配置USART奇偶校验
	usart_baudrate_set(USART0, 9600U);		      // 配置USART波特率
	usart_receive_config(USART0, USART_RECEIVE_ENABLE);   // USART接收器配置
	usart_transmit_config(USART0, USART_TRANSMIT_ENABLE); // USART发送器配置
	usart_enable(USART0);				      // 使能USART

	/*nvic_irq_enable(USART0_IRQn, 1, 0);		// 使能中断，配置中断的优先级
	usart_interrupt_enable(USART0, USART_INT_RBNE); // 使能“读数据缓冲区非空中断和过载错误中断”
	usart_interrupt_enable(USART0, USART_INT_IDLE); // 使能“IDLE线检测中断”
	usart_interrupt_enable(USART0, USART_INT_TBE);	// 使能“发送缓冲区空中断”*/
}
